import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';
import { DestinosApiClient } from '../models/destinos-api-client.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.scss']
})
export class ListaDestinosComponent implements OnInit {
  //destinos:DestinoViaje[];
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates:string[];

  constructor(
    public destinosApiClient:DestinosApiClient
  ) {
    //this.destinos=[];
    this.onItemAdded=new EventEmitter();
    this.updates=[];
    this.destinosApiClient.subscribeOnChange((d:DestinoViaje)=>{
      if(d!=null){
        this.updates.push('Se ha elegido a: '+d.nombre);
      }
    });
   }

  ngOnInit(): void {
  }

  guardar(nombre:string,url:string): boolean{
    let d = new DestinoViaje(nombre, url);
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
    return false;
  }

  agregado(d:DestinoViaje){
    //console.log(this.destinos);
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }

  elegido(d:DestinoViaje){
    //this.destinos.forEach( function(x){x.setSelected(false);} );
    //d.setSelected(true);
    //this.destinosApiClient.getAll().forEach(x => x.setSelected(false));
    //e.setSelected(true);
    this.destinosApiClient.elegir(d);
  }

  getAll(){

  }

}
